WallsHub
====================

[Get the app from Google Play](http://bit.ly/2kQRC6X)

WallsHub is an Android app that allows you to get wallpapers to your device, and also is free!

### Features
- New wallpapers every week.
- Ad-free!
- Simple to use
- Categories